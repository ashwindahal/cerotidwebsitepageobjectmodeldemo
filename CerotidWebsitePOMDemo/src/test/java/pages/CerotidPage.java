package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//Ashwin Nepal-- Added below Elements in hopes of understanding Page Object Model Framework 
//This class will return elements to the test class CerotidPageTest
public class CerotidPage {

	private static WebElement element = null;

	// Course Element
	public static WebElement selectCourse(WebDriver driver) {
		element = driver
				.findElement(By.xpath("//select[contains(@data-validation-required-message,'Please select Course.')]"));
		return element;

	}

	// Session Element
	public static WebElement selectSession(WebDriver driver) {
		element = driver.findElement(
				By.xpath("//select[contains(@data-validation-required-message,'Please select Session Date.')]"));
		return element;

	}

	// Full Name Element
	public static WebElement enterName(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[contains(@id,'name')]"));
		return element;

	}

	// Address Element
	public static WebElement enterAddress(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[contains(@id,'address')]"));
		return element;

	}

	// City Element
	public static WebElement enterCity(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[contains(@id,'city')]"));
		return element;

	}

	// State Element
	public static WebElement selectState(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[contains(@id,'state')]"));
		return element;

	}

	// Zip Element
	public static WebElement enterZip(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[contains(@id,'zip')]"));
		return element;

	}

	// Email Element
	public static WebElement enterEmail(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[contains(@id,'email')]"));
		return element;

	}

	// Phone Element
	public static WebElement enterPhone(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[contains(@id,'phone')]"));
		return element;

	}

	// Visa Element
	public static WebElement chooseVisa(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[contains(@id,'visaStatus')]"));
		return element;

	}

	// media source Element
	public static WebElement chooseMediaSource(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[contains(@id,'media')]"));
		return element;
	}

	// Relocate Yes Element
	public static WebElement relocateRadioBtn(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='relocate' and @value='Yes']"));
		return element;
	}

	// Education Form
	public static WebElement educationBGForm(WebDriver driver) {
		element = driver.findElement(By.xpath("//textarea[@id='eduDetails']"));
		return element;
	}

	// Education Form
	public static WebElement submitBtn(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@id='registerButton']"));
		return element;
	}

	// Education Form
	public static WebElement validateSuccessMessage(WebDriver driver) {
		element = driver.findElement(By.xpath("//strong[contains(text(),'Your register is completed. We will contact you shortly!')]"));
		return element;
	}

}
