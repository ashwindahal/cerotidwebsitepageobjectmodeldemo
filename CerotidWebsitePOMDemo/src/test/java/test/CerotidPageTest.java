package test;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import pages.CerotidPage;
//Ashwin Nepal-- Added below Actions to test the Cerotid Website and Understand Page Object Model Framework
public class CerotidPageTest {

	private static WebDriver driver = null;

	public static void main(String[] args) throws InterruptedException {
		invokeBrowserGoogleHomePage();
		fillForm();
		validateSuccessMessage();
		driver.quit();
	}

	public static void invokeBrowserGoogleHomePage() {

		System.setProperty("webdriver.chrome.driver", "C:\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		// Invoking Browser in Incognito Mode
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("incognito");
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);

		driver.navigate().to("http://www.cerotid.com");
		driver.navigate().refresh();
		System.out.println(driver.getTitle() + "----------------------------------------------------Was lauched");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	public static void fillForm() throws InterruptedException {

		// Utilizing the CerotidPage Objects to slect course
		Select chooseCourse = new Select(CerotidPage.selectCourse(driver));
		chooseCourse.selectByVisibleText("QA Automation");
		// Added sleeper to see the element interaction
		Thread.sleep(2000);

		// Utilizing the CerotidPage Objects to slect time to join session
		Select sessionDate = new Select(CerotidPage.selectSession(driver));
		sessionDate.selectByVisibleText("Upcoming Session");
		// Added sleeper to see the element interaction
		Thread.sleep(2000);

		// Entering name in name field
		CerotidPage.enterName(driver).sendKeys("John Doe");
		// Added sleeper to see the element interaction
		Thread.sleep(2000);

		// entering Address
		CerotidPage.enterAddress(driver).sendKeys("2300 Valley View Ln Suite 620, Irving, TX 75062");
		// Added sleeper to see the element interaction
		Thread.sleep(2000);

		// Entering City
		CerotidPage.enterCity(driver).sendKeys("Irving");
		// Added sleeper to see the element interaction
		Thread.sleep(2000);

		// Selecting State
		Select chooseState = new Select(CerotidPage.selectState(driver));
		chooseState.selectByVisibleText("TX");
		// Added sleeper to see the element interaction
		Thread.sleep(2000);

		// Entering zip
		CerotidPage.enterZip(driver).sendKeys("75062");
		// Added sleeper to see the element interaction
		Thread.sleep(2000);

		// Entering email address
		CerotidPage.enterEmail(driver).sendKeys("test@cerotid.com");
		// Added sleeper to see the element interaction
		Thread.sleep(2000);

		// Entering Phone Number
		CerotidPage.enterPhone(driver).sendKeys("(972)255-0046");
		// Added sleeper to see the element interaction
		Thread.sleep(2000);

		// Selecting via Source
		Select chooseVisaStatus = new Select(CerotidPage.chooseVisa(driver));
		chooseVisaStatus.selectByVisibleText("Other");
		// Added sleeper to see the element interaction
		Thread.sleep(2000);

		// Selecting medaia source
		Select chooseMediaSource = new Select(CerotidPage.chooseMediaSource(driver));
		chooseMediaSource.selectByVisibleText("Social Media");
		// Added sleeper to see the element interaction
		Thread.sleep(2000);

		// Clicking on Yes Radio Btn
		CerotidPage.relocateRadioBtn(driver).click();
		// Added sleeper to see the element interaction
		Thread.sleep(2000);

		// Entering Edu Form
		CerotidPage.educationBGForm(driver).sendKeys("Currently Learning at Cerotid");
		// Added sleeper to see the element interaction
		Thread.sleep(2000);

		// Clicking on submnit Button
		CerotidPage.submitBtn(driver).click();
		// Added sleeper to see the element interaction
		Thread.sleep(2000);
	}

	public static void validateSuccessMessage() {

		boolean isSuucessMessageDisplayed = false;
		String expectedMessage = "Your register is completed. We will contact you shortly!";
		String messageFromUI = CerotidPage.validateSuccessMessage(driver).getText();
		if (messageFromUI.equalsIgnoreCase(expectedMessage)) {
			isSuucessMessageDisplayed = true;
		}

		if (isSuucessMessageDisplayed) {
			System.out.println(
					"Pass- Expected Message 'Your register is completed. We will contact you shortly!' is displayed as Expected");
		} else {
			fail("Expected Message 'Your register is completed. We will contact you shortly!' is not displayed as Expected");
		}

	}
}
